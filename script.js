
let product = [];
let category = []

async function getCategory() {
    hideClass('.blocks');

    await axios.get(`https://fakestoreapi.com/products/categories`).then((response) => {
        category = response.data;
    })
    renderCategory();

}

async function getProductByCategory(category) {
    
    hideClass('.blocks');

    await axios.get(`https://fakestoreapi.com/products/category/${category}`).then((response) => {
        product = response.data;
    })
    
    openProductPage();
}

function hideClass(cl) {
    document.querySelector('.spinner').style.visibility = "visible";
    document.querySelector(cl).style.visibility = "hidden";
}
function showClass(cl) {
    document.querySelector('.spinner').style.visibility = "hidden";
    document.querySelector(cl).style.visibility = "visible";
}

function renderCategory () {
    const blocks = document.querySelector('.blocks')
    blocks.innerHTML = '';

    const fragment = document.createDocumentFragment()
    
    let list =  category;
    
    for ( let p of list) {
        
        const block = document.createElement('div');
        block.className = 'block shadow-lg rounded text-center'
        block.style.background = '--color-black';
        block.style.color = '#bbbcbf';

        const html = `<div>
                        <img class="rounded user-img" src="./assets/image/1.png" alt="">
                        <p class="category-name">${p}</p>
                     </div>`;  

        block.innerHTML = html

        block.addEventListener('click', () => {
            getProductByCategory (p);
            
            
        })

        fragment.appendChild(block)
    }
    blocks.appendChild(fragment)
    
    showClass('.blocks');

}

function openProductPage () {
    
    localStorage.removeItem('product');
    localStorage.setItem('product',JSON.stringify(product));
    location.href = "product.html";

}
let cart = [];

function reStoreCart() {
    
    cart =  JSON.parse(localStorage.getItem('cart'));
    refreshCardSum();
}

function storeCart() {
    
    localStorage.setItem('cart',JSON.stringify(cart));
}

function getProductItem(id) {
    return product.find(obj => {return obj.id == id;});
}

function addToCart(id) {
    cart.push (getProductItem(id));
    storeCart();
    refreshCardSum();
}

function deleteFromCart(id) {
for (let i = cart.length; i--; ) {
    if (cart[i].id == id) {
        cart.splice(i, 1);
        break;
    }
}
    storeCart();
    refreshCardSum();
    
}

function refreshCardSum() {
    let sum = cart.reduce((sum, current) =>{return sum + current.price},0)
    sum = getLocaleNum(sum); 
    document.querySelector('.cart-sum').innerHTML = `${cart.length} / ${sum}`;
}

function clearCart() {
    cart.length = 0;
    storeCart();
    refreshCardSum();
    document.querySelector('.blocks-cart').innerHTML = '';
}

function getLocaleNum(num) {
    return num.toLocaleString('ru', {style: "currency", currency: "eur"}); 
}

